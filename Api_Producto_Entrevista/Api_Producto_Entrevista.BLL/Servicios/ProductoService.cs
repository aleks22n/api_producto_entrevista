﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Api_Producto_Entrevista.BLL.Servicios.Contrato;
using Api_Producto_Entrevista.DAL.Repositorios.Contrato;
using Api_Producto_Entrevista.DTO;
using Api_Producto_Entrevista.Model;


namespace Api_Producto_Entrevista.BLL.Servicios
{
    public class ProductoService : IProductoService
    {
        private readonly IGenericRepository<Producto> _productoRepository;
        private readonly IMapper _mapper;

        public ProductoService(IGenericRepository<Producto> productoRepository, IMapper mapper)
        {
            _productoRepository = productoRepository;
            _mapper = mapper;
        }

        public async Task<List<ProductoDTO>> lista()
        {
            try
            {
                var ListaProducto = await _productoRepository.Consultar();
                return _mapper.Map<List<ProductoDTO>>(ListaProducto.ToList());
            }
            catch
            {
                throw;
            }
        }

        public async Task<ProductoDTO> Crear(ProductoDTO Modelo)
        {
            try
            {
               var ProductoCreado = await _productoRepository.Crear(_mapper.Map<Producto>(Modelo));
               
                if (ProductoCreado.ProductoId == 0)
                {
                    throw new TaskCanceledException("error, no se pudo crear");
                }
                return _mapper.Map<ProductoDTO>(ProductoCreado);
            }
            catch
            {

                throw;
            }
        }

        public async Task<bool> Editar(ProductoDTO Modelo)
        {
            try
            {
                var ProductoModelo = _mapper.Map<Producto>(Modelo);
                var ProductoEncontrado = await _productoRepository
                    .Obtener(u => u.ProductoId == ProductoModelo.ProductoId);

                if (ProductoEncontrado == null)
                {
                    throw new TaskCanceledException
                        ("error, el producto no existe");
                }

                ProductoEncontrado.Nombre = ProductoModelo.Nombre;
                ProductoEncontrado.Precio = ProductoModelo.Precio;
                ProductoEncontrado.Cantidad = ProductoModelo.Cantidad;

                bool respuesta = await _productoRepository
                    .Editar(ProductoEncontrado);

                if (!respuesta)
                {
                    throw new TaskCanceledException("error, no se pudo editar");
                }

                return respuesta;
            }
            catch
            {

                throw;
            }
        }

        public async Task<bool> Eliminar(int id)
        {
            try
            {
                var ProductoEncontrado = await _productoRepository
                    .Obtener(p => p.ProductoId == id);

                if (ProductoEncontrado == null)
                {
                    throw new TaskCanceledException("error, el producto no existe");
                }


                bool respuesta = await _productoRepository.Eliminar(ProductoEncontrado);

                if (!respuesta)
                {
                    throw new TaskCanceledException("error, no se pudo eliminar");
                }

                return respuesta;

            }
            catch
            {

                throw;
            }
        }
    }
}
