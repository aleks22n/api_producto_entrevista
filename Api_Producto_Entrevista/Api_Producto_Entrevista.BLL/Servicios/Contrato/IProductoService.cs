﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Api_Producto_Entrevista.DTO;

namespace Api_Producto_Entrevista.BLL.Servicios.Contrato
{
    public interface IProductoService
    {
        Task<List<ProductoDTO>> lista();

        Task<ProductoDTO> Crear(ProductoDTO Modelo);

        Task<bool> Editar(ProductoDTO Modelo);

        Task<bool> Eliminar(int id);
    }
}
