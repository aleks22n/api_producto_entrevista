﻿using System;
using System.Collections.Generic;

namespace Api_Producto_Entrevista.Model;

public partial class Producto
{
    public int ProductoId { get; set; }

    public string? Nombre { get; set; }

    public decimal? Precio { get; set; }

    public int? Cantidad { get; set; }
}
