﻿using Api_Producto_Entrevista.DAL.DBContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Api_Producto_Entrevista.DAL.Repositorios.Contrato;
using Api_Producto_Entrevista.DAL.Repositorios;

using Api_Producto_Entrevista.Utility;
using Api_Producto_Entrevista.BLL.Servicios.Contrato;
using Api_Producto_Entrevista.BLL.Servicios;

namespace Api_Producto_Entrevista.IOC
{
    public static class Dependencia
    {
        public static void InyectarDependencias(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DbEntrevistaProductoContext>(Options =>
            {
                Options.UseSqlServer(configuration
                    .GetConnectionString("DefaultConnection"));
            });

            services.AddTransient(typeof(IGenericRepository<>), typeof(GenericRepositiry<>));

            services.AddAutoMapper(typeof(AutoMapperProfile));



            services.AddScoped<DbContext, DbEntrevistaProductoContext>();

            services.AddScoped<IProductoService, ProductoService>();

        }
    }
}
