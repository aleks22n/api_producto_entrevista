﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Api_Producto_Entrevista.BLL.Servicios.Contrato;
using Api_Producto_Entrevista.DTO;
using Api_Producto_Entrevista.API.Utilidad;

namespace Api_Producto_Entrevista.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        private readonly IProductoService _ProductoService;

        public ProductoController(IProductoService productoService)
        {
            _ProductoService = productoService;
        }

        [HttpGet]
        [Route("Lista")]
        public async Task<ActionResult> Lista()
        {
            var RSP = new Response<List<ProductoDTO>>();
            try
            {
                RSP.Status = true;
                RSP.value = await _ProductoService.lista();
            }
            catch (Exception ex)
            {
                RSP.Status = false;
                RSP.msg = ex.Message;
            }
            return Ok(RSP);
        }

        [HttpPost]
        [Route("Guardar")]
        public async Task<IActionResult> Guardar([FromBody] ProductoDTO producto)
        {
            var RSP = new Response<ProductoDTO>();
            try
            {
                RSP.Status = true;
                RSP.value = await _ProductoService
                 .Crear(producto);
            }
            catch (Exception ex)
            {
                RSP.Status = false;
                RSP.msg = ex.Message;
            }
            return Ok(RSP);
        }

        [HttpPut]
        [Route("editar")]
        public async Task<IActionResult> editar([FromBody] ProductoDTO producto)
        {
            var RSP = new Response<bool>();
            try
            {
                RSP.Status = true;
                RSP.value = await _ProductoService
                 .Editar(producto);
            }
            catch (Exception ex)
            {
                RSP.Status = false;
                RSP.msg = ex.Message;
            }
            return Ok(RSP);
        }

        [HttpDelete]
        [Route("eliminar/{id:int}")]
        public async Task<IActionResult> eliminar(int id)
        {
            var RSP = new Response<bool>();
            try
            {
                RSP.Status = true;
                RSP.value = await _ProductoService
                 .Eliminar(id);
            }
            catch (Exception ex)
            {
                RSP.Status = false;
                RSP.msg = ex.Message;
            }
            return Ok(RSP);
        }
    }
}
 