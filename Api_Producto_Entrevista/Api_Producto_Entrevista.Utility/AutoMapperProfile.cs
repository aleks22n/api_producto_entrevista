﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;
using Api_Producto_Entrevista.DTO;
using Api_Producto_Entrevista.Model;

namespace Api_Producto_Entrevista.Utility
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Producto, ProductoDTO>().ReverseMap();
        }
    }
}
