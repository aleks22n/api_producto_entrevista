﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Api_Producto_Entrevista.Model;

namespace Api_Producto_Entrevista.DAL.DBContext;

public partial class DbEntrevistaProductoContext : DbContext
{
    public DbEntrevistaProductoContext()
    {
    }

    public DbEntrevistaProductoContext(DbContextOptions<DbEntrevistaProductoContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Producto> Productos { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Producto>(entity =>
        {
            entity.HasKey(e => e.ProductoId).HasName("PK__producto__A430AE831BE0FE24");

            entity.ToTable("productos");

            entity.Property(e => e.ProductoId).HasColumnName("ProductoID");
            entity.Property(e => e.Nombre)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Precio).HasColumnType("decimal(10, 2)");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
